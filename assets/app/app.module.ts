import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";

import {AppComponent} from "./app.component";
import {routing} from "./app.routing";
import sharedComponents from "./shared";
import messagesComponents from "./messages";
import authComponents from "./auth";
import {AuthService} from "./auth/auth.service";
import {ApiService} from "./shared/api.service";
import {MessageService} from "./messages/message.service";
import errorComponents from './errors';
import {ErrorService} from "./errors/error.service";

@NgModule({
    declarations: [
        AppComponent,
        ...sharedComponents,
        ...messagesComponents,
        ...authComponents,
        ...errorComponents
    ],
    imports: [
        BrowserModule,
        FormsModule,
        routing,
        ReactiveFormsModule,
        HttpModule
    ],
    providers: [AuthService, ApiService, MessageService, ErrorService],
    bootstrap: [AppComponent]
})
export class AppModule {

}