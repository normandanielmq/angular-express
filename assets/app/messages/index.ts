import {MessageComponent} from "./message.component";
import {MessageInputComponent} from "./message-input.component";
import {MessageListComponent} from "./message-list.component";
import {MessagesComponent} from "./messages.component";

export default [
    MessageComponent,
    MessageInputComponent,
    MessageListComponent,
    MessagesComponent
]