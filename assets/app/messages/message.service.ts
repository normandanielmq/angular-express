import {Message} from "./message.model";
import {Injectable, EventEmitter} from "@angular/core";
import {ApiService} from "../shared/api.service";

@Injectable()
export class MessageService {
    private messages: Message[] = [];
    messageIsEdit = new EventEmitter<Message>();

    constructor(private apiService: ApiService) {}

    addMessage(message: Message) {
        return this.apiService.request('message', 'post', message)
            .map(({obj: {_id, content, user: {_id: userId, firstName}}}: any) => {
                const message = new Message(
                    content,
                    firstName,
                    _id,
                    userId
                );
                this.messages.push(message);
                return message;
            });
    }

    getMessages() {
        return this.apiService.request('message')
            .map(({obj: messages}: any) => {
                let transformedMessages: Message[] = [];
                for (let {content, _id, user: {firstName, _id: userId}} of messages) {
                    transformedMessages.push(new Message(
                        content,
                        firstName,
                        _id,
                        userId
                    ));
                }
                this.messages = transformedMessages;
                return this.messages;
            });
    }

    editMessage(message: Message) {
        this.messageIsEdit.emit(message);
    }

    updateMessage(message: Message) {
        return this.apiService.request(`message/${message.messageId}`, 'patch', message);
    }

    deleteMessage(message: Message) {
        this.messages.splice(this.messages.indexOf(message), 1);
        return this.apiService.request(`message/${message.messageId}`, 'delete');
    }

}