import {Injectable} from "@angular/core";
import {Http, Headers, Response, RequestOptions, Request, URLSearchParams} from "@angular/http";
import 'rxjs/Rx';
import {Observable} from "rxjs";
import {ErrorService} from "../errors/error.service";

@Injectable()
export class ApiService {

    constructor(private http: Http, private errorService: ErrorService) {}

    get token() {
        return localStorage.getItem('token');
    }

    request(
        url: string,
        method = 'get',
        params?: Object
    ) {
        const options = new RequestOptions({url, method});
        options.headers = new Headers();
        if (this.token) {
            options.headers.append('token', this.token);
        }
        if (params) {
            if (['post', 'put', 'patch'].includes(method.toLowerCase())) {
                options.headers.append('Content-Type', 'application/json');
                options.body = JSON.stringify(params);
            } else {
                const queryString: URLSearchParams = new URLSearchParams();
                for (let key in params) {
                    queryString.set(key.toString(), params[key]);
                }
                options.search = queryString;
            }
        }
        return this.http.request(new Request(options))
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.errorService.handleError(error.json());
                return Observable.throw(error.json())
            });
    }
}