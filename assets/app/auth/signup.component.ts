import {Component, OnInit, Injectable} from "@angular/core";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {AuthService} from "./auth.service";
import {User} from "./user.model";

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html'
})
@Injectable()
export class SignupComponent implements OnInit {
    myForm: FormGroup;

    constructor(private authService: AuthService) {}

    onSubmit() {
        const {email, password, firstName, lastName} = this.myForm.value;
        const user = new User(email, password, firstName, lastName);
        this.authService.signup(user)
            .subscribe(
                result => console.log(result),
                error => console.error(error)
            );
        this.myForm.reset();
    }

    ngOnInit() {
        this.myForm = new FormGroup({
            firstName: new FormControl(null, Validators.required),
            lastName: new FormControl(null, Validators.required),
            email: new FormControl(null, [
                Validators.required,
                Validators.pattern("[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*")
            ]),
            password: new FormControl(null, Validators.required),
        });
    }

}