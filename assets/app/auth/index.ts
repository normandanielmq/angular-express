import {SigninComponent} from "./signin.component";
import {SignupComponent} from "./signup.component";
import {LogoutComponent} from "./logout.component";
import {AuthenticationComponent} from "./authentication.component";

export default [
    SigninComponent,
    SignupComponent,
    LogoutComponent,
    AuthenticationComponent
];