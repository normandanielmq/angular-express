import {User} from './user.model';
import {Injectable} from "@angular/core";
import {ApiService} from "../shared/api.service";

@Injectable()
export class AuthService {
    constructor(private apiService: ApiService) {}

    signup(user: User) {
        return this.apiService.request('user', 'post', user);
    }

    signin(user: User) {
        return this.apiService.request('user/signin', 'post', user);
    }

    logout() {
        localStorage.clear();
    }

    isLoggedIn() {
        return !!localStorage.getItem('token');
    }
}