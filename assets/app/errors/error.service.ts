import {EventEmitter} from "@angular/core";
import {Error} from "./error.model";

export class ErrorService {
    public errorOccurred = new EventEmitter<Error>();

    handleError({title, error: {message}}: any) {
        const errorData = new Error(title, message);
        this.errorOccurred.emit(errorData);
    }
}